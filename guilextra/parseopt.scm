;Copyright (C) 2018 by Todor Kondić


;;; Commentary:
;;;
;;; This module builds on getopt-long from ice-9 to provide some of
;;; the functionality of argparse Python module. It makes it easier to
;;; parse subcommands and attach functions that process their
;;; arguments.
;;;
;;; The command line is supposed to look like
;;; program [global - and -- options] [subcommand] [subcommand - and -- options] [non-option arguments] .
;;; Each subcommand gets a function to process the given options.
(define-module (guilextra parseopt)
  #:use-module (ice-9 getopt-long)
  #:use-module (srfi srfi-8)
  #:use-module (srfi srfi-1)
  #:export (make-entries
	    add-global-action-entry
	    add-action-entry
	    act)
  #:re-export (option-ref))


(define (proc-global-args args entries)
  "Process the global arguments subset of @var{args} given the
@var{entries} database and return two values, the first being the
assoc list containing global options and the second being the
unprocessed leftover of the initial @var{args} list."
  (receive (rest glob)
      (partition (lambda (m)
		   (eq? '() (car m)))
		 (let
		     ((global-opts (entry->opt-spec
				    (assq-ref entries '()))))
		   (getopt-long args global-opts #:stop-at-first-non-option #t)))
    (values glob (cdar rest))))


(define (entry->action-f entry)
  (assq-ref entry 'f))

(define (entry->opt-spec entry)
  (assq-ref entry 'opt-spec))

(define (proc-rest-args rest-args entries)
  "Detect the appropriate sub-command and process the arguments passed
to it. Return the name of the action as a symbol, the sub-command's
action function and the processed aruments. If no sub-command was
detected, return the global action name, the global action function
and #f instead of processed arguments."
  (let* ((glob-entry (assq '() entries))
	 (glob-f (entry->action-f (cdr glob-entry))))
    (if (not (null? rest-args))
	(begin
	  (let*
	      ((first (car rest-args))
	       (entry (assq (string->symbol first) entries)))
	    (if entry
		(let*
		    ((action-name (car entry))
		     (action-f (entry->action-f (cdr entry)))
		     (opt-spec (entry->opt-spec (cdr entry)))
		     (subcmd-args (getopt-long rest-args opt-spec)))
		  (values action-name action-f subcmd-args))
		(values '()
			glob-f
			'()))))
	(values '()
		glob-f
		'()))))

(define (act args entries)
  "Process the arguments @var{args} and call a corresponding function
based on the entries database @var{entries}."
  (let* (;Get processed global arguments.
	 (glob-stuff			
	  (receive (glob rest)
	      (proc-global-args args entries)
	    `(,glob . ,rest))))
    ;Obtain sub-command name, it's action function and the processed
    ;arguments of the sub-command.
    (receive (action action-f sub-args)
	(proc-rest-args (cdr glob-stuff) entries)
      ;; If a known sub-command was encountered in the command
      ;; line, call the corresponding action, else call the
      ;; global action.
      (if (not (eq? '()  action))
	  (action-f (car glob-stuff)
		    sub-args)
	  (action-f (car glob-stuff)
		    '())))))


(define (make-entries)
  "Create empty entries object."
  '())

(define (add-action-entry subcmd action-f opt-spec entries)
  "Register the symbol subcommand @var{subcmd} , its associated
processing function @var{action-f} and the getopt-long argument
specification @var{opt-spec} with the @var{entries} database. Returns
the updated entry database. Function @var{action-f} takes two
aruments, the first being the processed global options and the second
the processed subcmd options."
  `((,subcmd . ((f . ,action-f)
		(opt-spec . ,opt-spec)))
    ,@entries))



(define (add-global-action-entry action-f opt-spec entries)
  "Specify behaviour of the program when no sub-commands are
issued. Register the global action @var{action-f} with the associated
global option specificatoin @var{opt-spec} with the @var{entries}
database."
  (add-action-entry '() action-f opt-spec entries))
