;Copyright (C) 2018 by Todor Kondić

;;; Example of parseopt use.



(use-modules (guilextra parseopt))

(define entries (make-entries))

(define glob-spec
  `((help (single-char #\h)
	  (value #f))
    (version (single-char #\v)
	     (value #f))))


(define (glob-f garg sarg)
  (let
      ((help (option-ref garg 'help #f))
       (ver (option-ref garg 'version #f)))
    (cond
     (help
      (display "Available commands:")
      (newline)
      (for-each (lambda (m)
		  (display (car m))
		  (newline))
		entries))
     (ver
      (display "Version: 0.0.0")
      (newline))
     (#t (display "De Nada\n")))))


(define cmd-print-spec
  `((help (single-char #\h))))

(define (cmd-print-f garg sarg)
  (let
      ((help (option-ref sarg 'help #f))
       (nonopt (option-ref sarg '() #f)))
    (cond
     (help
      (display "Prints arguments")
      (newline)
      (quit))
     (nonopt
      (display nonopt) 
      (newline)))))




(set! entries (add-global-action-entry glob-f glob-spec entries))
(set! entries (add-action-entry 'print cmd-print-f cmd-print-spec entries))


(act (command-line) entries)

 
